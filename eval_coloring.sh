#!/bin/bash
source /scratch/bachmanp/3-coloring-circle-graphs/code/venv/bin/activate
evaluate_coloring "$1" "$2"
